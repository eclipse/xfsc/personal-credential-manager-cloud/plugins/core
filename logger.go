package core

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
	"log"
)

var logger = getLogger()

func getLogger() types.Logger {
	l, err := core.NewLogger(libConfig.LogLevel, libConfig.IsDev)
	if err != nil {
		log.Fatal(err)
	}
	return *l
}
